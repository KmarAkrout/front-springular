export interface Database {
  id?:number
  nameDatabase:string
  typeDatabase:string
  usernameDatabase:string
  passwordDatabase:string
  project_id:number
}
