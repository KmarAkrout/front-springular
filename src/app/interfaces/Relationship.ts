import { Entity } from './entity';

export interface Relationship {
  id?: number;
  parentEntity_id: number;
  childEntity_id: number;
  typeRelationship: string;
  parentEntity?: Entity;
  childEntity?: Entity;
}
