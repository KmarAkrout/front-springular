import { Database } from './database';

export interface Project {
  id?:number
  nameProject:string
  portProject:number
  pathProject:string
  typeProject:string
  usernameProject:string
  database?:Database
}
