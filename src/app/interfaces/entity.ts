import { Attribute } from './attribute';
import { Project } from './project';

export interface Entity {
  id?: number;
  attributes: Attribute[];
  nameEntity: string;
  isTrackable: boolean;
  createListIdsIfChild: boolean;
  createListDtosIfChild: boolean;
  project_id: number;
  disabled?: boolean;
  project?: Project;
}
