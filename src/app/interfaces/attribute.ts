
export interface Attribute {
  id?:number
  nameAttribute:string
  typeAttribute:string
  entity_id?:number
}
