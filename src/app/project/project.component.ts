import { Component, OnInit } from '@angular/core';
import { ProjectService } from './project.service';
import { Project } from '../interfaces/project';
import { Router } from '@angular/router';
import { MatTableDataSource } from '@angular/material/table';
import { EntityService } from '../entity/entity.service';
import { Entity } from '../interfaces/entity';
import { MatDialog } from '@angular/material/dialog';
import { AddProjectComponent } from '../dialog-modal/add-project/add-project.component';
import { DeleteModalComponent } from '../dialog-modal/delete-modal/delete-modal.component';
import { DomSanitizer } from '@angular/platform-browser';
import * as FileSaver from 'file-saver';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.scss']
})
export class ProjectComponent implements OnInit {
  displayedColumns: string[] = ['id', 'nameProject', 'portProject', 'typeProject'];
  projects: Project[] = [];
  dataSource;
  entities: Entity[] = [];
  isDisplayed = true;
  fileUrl;

  constructor(private sanitizer: DomSanitizer, private service: ProjectService, private router: Router, private entityService: EntityService, private projectDialog: MatDialog) {
  }

  ngOnInit() {
    this.getProjects();
  }

  getProjects() {
    this.service.getAllProjects().subscribe(data => {
      this.projects = data;
      this.dataSource = new MatTableDataSource(this.projects);
    });
  }

  deleteProject(project) {
    /*this.service.deleteProject(project.id).subscribe(data => {
      this.projects.splice(this.projects.indexOf(project), 1);
    });*/
    const dialogRef = this.projectDialog.open(DeleteModalComponent, {
      width: '650px',
      data: {elementId: project.id, elementType: 'project'}
    });
    dialogRef.afterClosed().subscribe(result => {
      this.getProjects();
    });

  }

  updateProject(project) {
    const dialogRef = this.projectDialog.open(AddProjectComponent, {
      width: '800px',
      data: {projectId: project.id}
    });
    dialogRef.afterClosed().subscribe(result => {
      this.getProjects();
    });
  }

  showProjectDetails(project) {
    if (project.id != null) {
      this.router.navigate(['/entities/project', project.id]);
    }

  }

  addProject() {
    const dialogRef = this.projectDialog.open(AddProjectComponent, {
      width: '800px'
    });
    dialogRef.afterClosed().subscribe(result => {
      this.getProjects();
    });
  }
  /*generateProject(project) {
    this.service.generateProject(project.id).subscribe(data=>{
      const blob = new Blob([data._body], { type: 'application/octet-stream' });
      this.fileUrl = this.sanitizer.bypassSecurityTrustResourceUrl
      (window.URL.createObjectURL(blob));
    })
  }*/
  downloadFile(data: any,project:Project) {
    const blob = new Blob([data], { type: 'application/zip' });
    const fileName = project.nameProject.concat(".zip");
    FileSaver.saveAs(blob, fileName);
  }
  generateProject(project) {
    this.service.generateProject(project.id).subscribe(respData => {
      this.downloadFile(respData, project);
    }, error => {
      console.log(error);
    });
  }

}
