import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { Project } from '../interfaces/project';
import { Database } from '../interfaces/database';
import { Content } from '@angular/compiler/src/render3/r3_ast';


@Injectable()
export class ProjectService {
  constructor(private http: HttpClient) {
  }

  getProjectById(id: number): Observable<Project> {
    return this.http.get<Project>(environment.DISTANT_BACKEND_URL + 'project/' + id);
  }
  getAllProjects(): Observable<Project[]> {
    return this.http.get<Project[]>(environment.DISTANT_BACKEND_URL + 'projects');
  }
   saveProject(project: Project): Observable<Project> {
    return this.http.post<Project>(environment.DISTANT_BACKEND_URL + 'project', project);
  }
  updateProject(id: number, project: Project): Observable<Project> {
    return this.http.put<Project>(environment.DISTANT_BACKEND_URL + 'project/' + id, project);
  }
  updateDatabase(id: number, body: Database): Observable<Database> {
    return this.http.put<Database>(environment.DISTANT_BACKEND_URL + 'database/' + id, body);
  }
  deleteProject(id: number): Observable<Project> {
    return this.http.delete<Project>(environment.DISTANT_BACKEND_URL + 'project/' + id);
  }
  saveDatabase(database: Database): Observable<Database> {
    return this.http.post<Database>(environment.DISTANT_BACKEND_URL + 'database', database);
  }
  generateProject(id: number): Observable<any> {
    const options: any = {
      headers: new HttpHeaders({'Content-Type': 'application/octet-stream'}),
      withCredentials: false,
      responseType: 'arraybuffer'
    };
    return this.http.get(environment.DISTANT_BACKEND_URL + 'generation/project/' + id, options);
  }


}
