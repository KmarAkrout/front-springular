import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomePageComponent } from './home-page/home-page.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { MatCardModule, MatCheckboxModule, MatDialogModule, MatProgressSpinnerModule, MatTableModule } from '@angular/material';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { CompareDialogComponent } from './compare-dialog/compare-dialog.component';
import { ToastrModule } from 'ngx-toastr';
import { ProjectComponent } from './project/project.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProjectService } from './project/project.service';
import { MatStepperModule } from '@angular/material/stepper';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { EntityComponent } from './entity/entity.component';
import { EntityService } from './entity/entity.service';
import { DialogModalComponent } from './dialog-modal/dialog-modal.component';
import { AddAttributeComponent } from './dialog-modal/add-attribute/add-attribute.component';
import { AddEntityComponent } from './dialog-modal/add-entity/add-entity.component';
import { AddRelationshipComponent } from './dialog-modal/add-relationship/add-relationship.component';
import { AddProjectComponent } from './dialog-modal/add-project/add-project.component';
import { DeleteModalComponent } from './dialog-modal/delete-modal/delete-modal.component';
import { LoaderService } from './core/interceptors/loader.service';
import { LoaderInterceptor } from './core/interceptors/loader-interceptor';
import { LoaderComponent } from './loader/loader.component';


@NgModule({
  declarations: [
    AppComponent, AddEntityComponent, HomePageComponent, CompareDialogComponent,
    ProjectComponent, AddProjectComponent, EntityComponent, DialogModalComponent, AddAttributeComponent, AddRelationshipComponent, DeleteModalComponent, LoaderComponent
  ],
  imports: [
    BrowserModule, MatFormFieldModule, MatInputModule, MatStepperModule, BrowserAnimationsModule, LayoutModule, MatToolbarModule,
    MatButtonModule, MatSidenavModule, MatIconModule, MatListModule, AppRoutingModule, MatTableModule, MatCheckboxModule, HttpClientModule,
    MatDialogModule, MatCardModule, ToastrModule.forRoot({
      timeOut: 5000,
      positionClass: 'toast-top-right',
      preventDuplicates: true,
      closeButton: true
    }), ReactiveFormsModule, MatSelectModule, FormsModule, MatProgressSpinnerModule

  ],
  providers: [HttpClientModule, ProjectService, EntityService, LoaderService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LoaderInterceptor,
      multi: true
    }],
  entryComponents: [
    CompareDialogComponent, DeleteModalComponent, DialogModalComponent, AddAttributeComponent, AddEntityComponent, AddRelationshipComponent, AddProjectComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
