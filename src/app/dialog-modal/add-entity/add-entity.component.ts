import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Entity } from '../../interfaces/entity';
import { Attribute } from '../../interfaces/attribute';
import { EntityService } from '../../entity/entity.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { AddAttributeComponent } from '../add-attribute/add-attribute.component';

@Component({
  selector: 'app-add-entity',
  templateUrl: './add-entity.component.html',
  styleUrls: ['./add-entity.component.scss']
})
export class AddEntityComponent implements OnInit {
  entityFormGroup: FormGroup;
  projectId;

  @ViewChild(AddAttributeComponent, {static: false}) attributeComponent: AddAttributeComponent;

  constructor(private _formBuilder: FormBuilder, private entityService: EntityService, @Inject(MAT_DIALOG_DATA) public data: any,
              public dialogRef: MatDialogRef<AddEntityComponent>) {
    this.projectId = data.projectId;
  }

  ngOnInit() {
    this.entityFormGroup = this._formBuilder.group({
      entityName: ['', Validators.required],
      isTrackable: [false, Validators.required]
    });

  }

  addEntity() {
    let entity: Entity = {
      attributes: this.attributeComponent.addAttributeToTable(),
      createListDtosIfChild: true,
      createListIdsIfChild: false,
      isTrackable: this.entityFormGroup.get('isTrackable').value,
      nameEntity: this.entityFormGroup.get('entityName').value,
      project_id: this.projectId
    };
    this.entityService.saveEntity(entity).subscribe(data => {
      this.dialogRef.close();
    });
  }
}

