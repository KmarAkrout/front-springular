import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ProjectService } from '../../project/project.service';
import { EntityService } from '../../entity/entity.service';

@Component({
  selector: 'app-delete-modal',
  templateUrl: './delete-modal.component.html',
  styleUrls: ['./delete-modal.component.css']
})
export class DeleteModalComponent implements OnInit {

  elementType;
  elementId;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, public dialogRef: MatDialogRef<DeleteModalComponent>,
              public projectService: ProjectService, public entityService: EntityService) {
    this.elementType = data.elementType;
    this.elementId = data.elementId;
  }

  ngOnInit() {
  }

  delete() {
switch (this.elementType) {
  case 'project':
    this.projectService.deleteProject(this.elementId).subscribe(data => {
      this.dialogRef.close();
    }); break;

  case 'entity':
    this.entityService.deleteEntity(this.elementId).subscribe(data => {
      this.dialogRef.close();
    });break;

}
  }

  cancel() {
    this.dialogRef.close();

  }
}
