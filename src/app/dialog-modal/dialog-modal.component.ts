import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Entity } from '../interfaces/entity';

@Component({
  selector: 'app-dialog-modal',
  templateUrl: './dialog-modal.component.html',
  styleUrls: ['./dialog-modal.component.scss']
})
export class DialogModalComponent implements OnInit {
  displayedColumns: string[] = ['attributeName', 'attributeType'];
  dataSource;
  displayedEntity: Entity;
  constructor( public dialogRef: MatDialogRef<DialogModalComponent>, @Inject(MAT_DIALOG_DATA) public data: any) {
    this.displayedEntity = this.data.entityDetails;
    this.dataSource = this.data.entityDetails;
  }
  ngOnInit() {
  }
  cancel() {
    this.dialogRef.close();
  }
}
