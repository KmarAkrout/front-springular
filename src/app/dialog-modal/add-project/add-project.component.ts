import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Project } from '../../interfaces/project';
import { Database } from '../../interfaces/database';
import { ProjectService } from '../../project/project.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-add-project',
  templateUrl: './add-project.component.html',
  styleUrls: ['./add-project.component.scss']
})
export class AddProjectComponent implements OnInit {

  isLinear = false;
  projectFormGroup: FormGroup;
  databaseFormGroup: FormGroup;
  hide = true;
  projectId;
  projectToBeUpdated: Project;
  databaseToBeUpdated: Database;
  isUpdate = false;
  databaseId: number;

  constructor(private service: ProjectService, private _formBuilder: FormBuilder, private route: ActivatedRoute, private router: Router, @Inject(MAT_DIALOG_DATA) public data: any,
              public dialogRef: MatDialogRef<AddProjectComponent>) {

    if (data !== null && data.projectId !== null) {
      this.projectId = data.projectId;
    }
  }

  ngOnInit() {
    if (this.projectId != null) {
      this.isUpdate = true;
      this.service.getProjectById(this.projectId).subscribe(data => {
        this.projectToBeUpdated = data;
        console.log(data);
        if (data.database !== null) {
          this.databaseToBeUpdated = {
            project_id: this.projectId,
            nameDatabase: data.database.nameDatabase,
            passwordDatabase: data.database.passwordDatabase,
            typeDatabase: data.database.typeDatabase,
            usernameDatabase: data.database.usernameDatabase
          };
          this.databaseFormGroup.patchValue({
            databasename: this.databaseToBeUpdated.nameDatabase,
            databaseusername: this.databaseToBeUpdated.usernameDatabase,
            databasepassword: this.databaseToBeUpdated.passwordDatabase,
            databasetype: this.databaseToBeUpdated.typeDatabase

          });
          this.databaseId = data.database.id;
        }
        this.projectFormGroup.setValue({
          projectName: this.projectToBeUpdated.nameProject,
          projectport: this.projectToBeUpdated.portProject,
          projecttype: this.projectToBeUpdated.typeProject
        });
      });

    }
    this.projectFormGroup = this._formBuilder.group({
      projectName: ['', Validators.required],
      projectport: ['', Validators.required],
      projecttype: ['', Validators.required]
    });
    this.databaseFormGroup = this._formBuilder.group({
      databasename: [''],
      databasetype: [''],
      databaseusername: [''],
      databasepassword: ['']
    });
  }

  saveOrUpdate() {
    this.isUpdate ? this.updateProject() : this.createProject();
  }

  updateProject() {
    const project = this.preparedProjectDataForUpdateProject();
    const database = this.preparedDatabaseDataForUpdateProject();
    this.service.updateProject(this.projectId, project).subscribe(data => {
      if (project.database != null) {
        this.service.updateDatabase(this.databaseId, database).subscribe(data => {
        });
      } else {
        database.project_id = this.projectId;
        this.service.saveDatabase(database).subscribe(data => {
        });
      }
      this.dialogRef.close();
      this.service.getAllProjects().subscribe();
    });

  }

  private preparedProjectDataForUpdateProject() {
    const project: Project = {
      nameProject: this.projectFormGroup.get('projectName').value,
      pathProject: '/users',
      portProject: this.projectFormGroup.get('projectport').value,
      typeProject: this.projectFormGroup.get('projecttype').value,
      usernameProject: 'sifast-project'
    };
    return project;
  }

  private preparedDatabaseDataForUpdateProject() {
    const database: Database = {
      nameDatabase: this.databaseFormGroup.get('databasename').value,
      passwordDatabase: this.databaseFormGroup.get('databasepassword').value,
      project_id: this.projectId,
      typeDatabase: this.databaseFormGroup.get('databasetype').value,
      usernameDatabase: this.databaseFormGroup.get('databaseusername').value
    };
    return database;
  }

  createProject() {
    const project = this.prepareProjectData();
    this.service.saveProject(project).subscribe(data => {
      const database = this.prepareDatabaseData(data);
      this.service.saveDatabase(database).subscribe(() => {
      });
      this.dialogRef.close();
    });
  }

  private prepareDatabaseData(data: Project) {
    const database: Database = {
      nameDatabase: this.databaseFormGroup.get('databasename').value,
      typeDatabase: this.databaseFormGroup.get('databasetype').value,
      usernameDatabase: this.databaseFormGroup.get('databaseusername').value,
      passwordDatabase: this.databaseFormGroup.get('databasepassword').value,
      project_id: data.id
    };
    return database;
  }

  private prepareProjectData() {
    const project: Project = {
      nameProject: this.projectFormGroup.get('projectName').value,
      portProject: this.projectFormGroup.get('projectport').value,
      pathProject: '/users',
      typeProject: this.projectFormGroup.get('projecttype').value,
      usernameProject: 'sifast-project'
    };
    return project;
  }
}
