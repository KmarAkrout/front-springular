import { Component, Inject, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Attribute } from '../../interfaces/attribute';
import { MatTableDataSource } from '@angular/material/table';
import { EntityService } from '../../entity/entity.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-add-attribute',
  templateUrl: './add-attribute.component.html',
  styleUrls: ['./add-attribute.component.scss']
})
export class AddAttributeComponent implements OnInit {
  displayedColumns: string[] = ['nameAttribute', 'typeAttribute'];
  isDisplayed = false;
  dataSource;
  attributeFormGroup: FormGroup;
  attributesToPutInTable: Attribute[] = [];
  selectedItem;
  attribute: Attribute = {
    nameAttribute: '',
    typeAttribute: ''
  };
  actualEntityId: number;
  @Input() displayButton = true;


  constructor(private _formBuilder: FormBuilder, private entityService: EntityService , @Inject(MAT_DIALOG_DATA) public data: any, dialogRef: MatDialogRef<AddAttributeComponent>) {
    this.actualEntityId = data.entityId;
  }

  ngOnInit() {
    this.attributeFormGroup = this._formBuilder.group({
      attributeName: [''],
      attributeType: ['']
    });
  }

  addAttributeToTable(): Attribute[] {
    if (this.attributeFormGroup.get('attributeName').value !== '') {
      this.attribute = {
        nameAttribute: this.attributeFormGroup.get('attributeName').value,
        typeAttribute: this.attributeFormGroup.get('attributeType').value,
        entity_id: this.actualEntityId
      };
      this.attributesToPutInTable.push(this.attribute);
      this.attributeFormGroup.patchValue({
        attributeName : '',
        attributeType : ''
      })
      this.isDisplayed = true;
      this.dataSource = new MatTableDataSource(this.attributesToPutInTable);
    }
    return this.attributesToPutInTable;
  }

  addAttributesToEntity() {
    let attributesToAdd: Attribute[] = [];
    attributesToAdd = this.attributesToPutInTable;
    attributesToAdd.splice(attributesToAdd.length, 1);
    attributesToAdd.forEach(attribute => {
      this.entityService.saveAttribute(attribute).subscribe(data => {
      });
    });

  }
}
