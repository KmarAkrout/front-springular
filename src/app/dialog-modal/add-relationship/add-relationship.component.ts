import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EntityService } from '../../entity/entity.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Entity } from '../../interfaces/entity';
import { Relationship } from '../../interfaces/Relationship';

@Component({
  selector: 'app-add-relationship',
  templateUrl: './add-relationship.component.html',
  styleUrls: ['./add-relationship.component.scss']
})
export class AddRelationshipComponent implements OnInit {

  parentEntity: Entity;
  childEntity: Entity;
  relationshipFormGroup: FormGroup;
  relationshipsTypes = ['OneToMany', 'ManyToMany', 'OneToOne'];

  constructor(private _formBuilder: FormBuilder, private entityService: EntityService, @Inject(MAT_DIALOG_DATA) public data: any,
              public dialogRef: MatDialogRef<AddRelationshipComponent>) {
    this.parentEntity = data.entitiesChecked[0];
    this.childEntity = data.entitiesChecked[1];

  }

  ngOnInit() {

    this.relationshipFormGroup = this._formBuilder.group({
      parentEntity: ['', Validators.required],
      childEntity: ['', Validators.required],
      type: ['']
    });
    this.relationshipFormGroup.patchValue({
      parentEntity: this.parentEntity.nameEntity,
      childEntity: this.childEntity.nameEntity
    });
  }

  swapEntities() {
    this.relationshipFormGroup.patchValue({
      parentEntity: this.relationshipFormGroup.get('childEntity').value,
      childEntity: this.relationshipFormGroup.get('parentEntity').value
    });
    let aux = this.parentEntity;
    this.parentEntity = this.childEntity;
    this.childEntity = aux;
  }

  addRelationship() {
    const relationship: Relationship = {
      childEntity_id: this.childEntity.id,
      parentEntity_id: this.parentEntity.id,
      typeRelationship: this.relationshipFormGroup.get('type').value

    };
    this.entityService.saveRelationship(relationship).subscribe(data => {
      this.dialogRef.close();
    });
  }
}
