import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { Entity } from '../interfaces/entity';
import { Attribute } from '../interfaces/attribute';
import { Relationship } from '../interfaces/Relationship';

@Injectable()
export class EntityService {
  constructor(private http: HttpClient) {
  }

  getEntityById(id: number): Observable<Entity> {
    return this.http.get<Entity>(environment.DISTANT_BACKEND_URL + 'buisnessLogicEntity/' + id);
  }

  getEntitiesByProjectId(id: number): Observable<Entity[]> {
    return this.http.get<Entity[]>(environment.DISTANT_BACKEND_URL + 'buisnessLogicEntity/project/' + id);
  }

  getAllEntities(): Observable<Entity[]> {
    return this.http.get<Entity[]>(environment.DISTANT_BACKEND_URL + 'buisnessLogicEntitys');
  }

  saveEntity(entity: Entity): Observable<Entity> {
    return this.http.post<Entity>(environment.DISTANT_BACKEND_URL + 'buisnessLogicEntity', entity);
  }

  updateEntity(id: number, entity: Entity): Observable<Entity> {
    return this.http.put<Entity>(environment.DISTANT_BACKEND_URL + 'buisnessLogicEntity/' + id, entity);
  }

  deleteEntity(id: number): Observable<Entity> {
    return this.http.delete<Entity>(environment.DISTANT_BACKEND_URL + 'buisnessLogicEntity/' + id);
  }

  saveRelationship(relation: Relationship): Observable<Relationship> {
    return this.http.post<Relationship>(environment.DISTANT_BACKEND_URL + 'relationship', relation);
  }
  saveAttribute(attribute: Attribute): Observable<Attribute> {
    return this.http.post<Attribute>(environment.DISTANT_BACKEND_URL + 'attribute', attribute);
  }

  getRelationshipsByProjectId(id: number): Observable<Relationship[]> {
    return this.http.get<Relationship[]>(environment.DISTANT_BACKEND_URL + 'relationship/project/' + id);
  }
}
