import { Component, OnInit } from '@angular/core';
import { ProjectService } from '../project/project.service';
import { EntityService } from './entity.service';
import { Entity } from '../interfaces/entity';
import { MatTableDataSource } from '@angular/material/table';
import { Project } from '../interfaces/project';
import { DialogModalComponent } from '../dialog-modal/dialog-modal.component';
import { MatDialog } from '@angular/material/dialog';
import { AddAttributeComponent } from '../dialog-modal/add-attribute/add-attribute.component';
import { ActivatedRoute } from '@angular/router';
import { AddEntityComponent } from '../dialog-modal/add-entity/add-entity.component';
import { ToastrService } from 'ngx-toastr';
import { AddRelationshipComponent } from '../dialog-modal/add-relationship/add-relationship.component';
import { Relationship } from '../interfaces/Relationship';
import { DeleteModalComponent } from '../dialog-modal/delete-modal/delete-modal.component';

@Component({
  selector: 'app-entity',
  templateUrl: './entity.component.html',
  styleUrls: ['./entity.component.scss']
})
export class EntityComponent implements OnInit {
  project: Project;
  projectList: Project[] = [];
  displayedColumns: string[] = ['check', 'nameEntity', 'isTrackable', 'project_id', 'actions', 'addAttribute'];
  displayedRelationshipsColumns: string[] = ['type', 'parent', 'child', 'actions'];
  dataSource;
  entities: Entity[] = [];
  isDisplayed = false;
  actualEntityId;
  projectId;
  isCreateRelation = false;
  checkedEntities: Entity[] = [];
  result = false;
  isChecked = false;
  dataSourceRelationships;
  relationships: Relationship[] = [];

  constructor(private projectService: ProjectService, private entityService: EntityService, private entityDialog: MatDialog, private route: ActivatedRoute,
              private toastr: ToastrService) {
  }

  ngOnInit() {
    this.projectId = this.route.snapshot.paramMap.get('id');
    if (this.projectId != null) {
      this.getEntitiesByProjectId(this.projectId);
      this.getRelationshipsByProjectId(this.projectId);
      this.projectService.getProjectById(this.projectId).subscribe(data => {
        this.project = {
          nameProject: data.nameProject,
          pathProject: data.pathProject,
          portProject: data.portProject,
          typeProject: data.typeProject,
          usernameProject: data.usernameProject
        };
        if (data.database != null) {
          this.project.database = data.database;
        }
      });
    }

  }

  private initProjectList() {
    this.projectService.getAllProjects().forEach(data => {
      data.forEach(element => {
        this.projectList.push(element);
      });
    });
  }

  getEntitiesByProjectId(id) {
    this.entityService.getEntitiesByProjectId(id).subscribe(data => {
      data.map(element => element.disabled = false);
      this.entities = data;
      if (this.entities.length > 0 && this.entities !== null) {
        this.isDisplayed = true;
        this.entities.forEach(element => {
          element.project_id = id;
        });
        this.dataSource = new MatTableDataSource(this.entities);
      } else {
        this.isDisplayed = false;
      }
    });
  }

  getRelationshipsByProjectId(id) {
    this.entityService.getRelationshipsByProjectId(id).subscribe(data => {
      this.relationships = data;
      this.dataSourceRelationships = this.relationships;
    });
  }

  showEntityDetailsaction(id: any) {
    this.entityService.getEntityById(id).subscribe(data => {
      const dialogRef = this.entityDialog.open(DialogModalComponent, {
        width: '800px',
        data: {entityDetails: data}
      });
      dialogRef.afterClosed().subscribe(result => {

      });

    });
    this.actualEntityId = id;
  }

  editEntityAction(element: any) {

  }

  deleteEntity(entity) {
    if (this.relationships.some(element => element.parentEntity.id === entity.id || element.childEntity.id === entity.id)) {
      this.toastr.error('you can\'t delete an entity that has a relationship with another');
    } else {
      const dialogRef = this.entityDialog.open(DeleteModalComponent, {
        width: '700px',
        data: {elementId: entity.id, elementType: 'entity'}
      });
      dialogRef.afterClosed().subscribe(result => {
        this.getEntitiesByProjectId(this.projectId);
        this.dataSource = new MatTableDataSource(this.entities);
      });
    }
  }

  addAttribute(id) {
    this.actualEntityId = id ;
    const dialogRef = this.entityDialog.open(AddAttributeComponent, {
      width: '800px',
      data: {entityId: this.actualEntityId}
    });
    dialogRef.afterClosed().subscribe(result => {
    });
  }

  chooseEntity(event, entity) {
    if (!event.checked) {
      this.entities.map(element => element.disabled = false);
      this.checkedEntities.splice(entity, 1);
      if (this.checkedEntities.length === 1) {
        this.isCreateRelation = true;
      } else {
        this.isCreateRelation = false;
      }
    }
    if (event.checked && this.checkedEntities.length < 2) {
      this.checkedEntities.push(entity);
      this.isCreateRelation = true;
      if (this.checkedEntities.length === 1) {
        this.result = true;
        this.isCreateRelation = false;
      }
    }
    if (this.checkedEntities.length === 2) {
      this.entities = this.entities.filter(element => !this.isCheckedEntity(element));
      this.entities.map(element => element.disabled = true);
    }
  }

  isCheckedEntity(entity): boolean {
    return this.checkedEntities.some(element => element === entity);
  }

  addEntity() {
    const dialogRef = this.entityDialog.open(AddEntityComponent, {
      width: '800px',
      data: {projectId: this.projectId}
    });
    dialogRef.afterClosed().subscribe(result => {
      this.getEntitiesByProjectId(this.projectId);
    });
  }

  createRelationship() {
    if (this.result) {
      const dialogRef = this.entityDialog.open(AddRelationshipComponent, {
        width: '800px',
        data: {entitiesChecked: this.checkedEntities}
      });
      dialogRef.afterClosed().subscribe(result => {
        this.isCreateRelation = false;
        this.isChecked = false;
        this.getRelationshipsByProjectId(this.projectId);
        this.entities.map(element => element.disabled = false);
      });
    }
  }

  editRelationship(element) {

  }

  deleteRelationship(element) {

  }

}
