import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
@Injectable()
export class LoaderService {
  loader = false;

  setLoader(value) {
    this.loader = value;
  }
  getLoader(): Observable<boolean> {
    return of(this.loader);
  }
}
